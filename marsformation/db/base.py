from sqlalchemy.orm import DeclarativeBase

from marsformation.db.meta import meta


class Base(DeclarativeBase):
    """Base for all models."""

    metadata = meta
