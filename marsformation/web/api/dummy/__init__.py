"""Dummy model API."""
from marsformation.web.api.dummy.views import router

__all__ = ["router"]
