"""Echo API."""
from marsformation.web.api.echo.views import router

__all__ = ["router"]
