"""API for checking project status."""
from marsformation.web.api.monitoring.views import router

__all__ = ["router"]
