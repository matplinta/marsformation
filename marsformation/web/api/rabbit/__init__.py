"""API to interact with RabbitMQ."""
from marsformation.web.api.rabbit.views import router

__all__ = ["router"]
